<?php

namespace KITT3N\Pimcore\FulltextSearchBundle\Service;

/**
 * Class FulltextSearchService
 *
 * @package KITT3N\Pimcore\FulltextSearchBundle\Service
 *
 * @deprecated
 */
class FulltextSearchService
{
    /**
     * @param string            $s
     * @param array|\string[][] $aTypeDefinitions
     * @param array|string[]    $aFulltextColumns
     * @param string            $sIndex
     *
     * @return array
     *
     * @deprecated
     */
    public function getFulltextSearchResults (
        string $s,
        array $aTypeDefinitions = [[ 'maintype' => 'document', 'type' => 'page', 'subtype' => 'page' ]],
        array $aFulltextColumns = ['data', 'properties'],
        string $sIndex = 'search_backend_data'
    ) {

        $aMaintypes = [];
        $aTypes = [];
        $aSubtypes = [];
        foreach ($aTypeDefinitions as $aTypeDefinition) {
            $aMaintypes[] = $aTypeDefinition['maintype'];
            $aTypes[] = $aTypeDefinition['type'];
            $aSubtypes[] = $aTypeDefinition['subtype'];
        }

        $aS = [];
        $aSQuoted = [];
        $aSHighlight = [];
        $aResults = [];

        /*
         * Remove all characters not matching:
         *
         * \w match any word character in any script (equal to [\p{L}\p{N}_])
         * \s matches any kind of invisible character (equal to [\p{Z}\h\v])
         * \& matches the character & literally (case sensitive)
         * \/ matches the character / literally (case sensitive)
         * \" matches the character " literally (case sensitive)
         * \- matches the character - literally (case sensitive)
         * \+ matches the character + literally (case sensitive)
         *
         * + Quantifier — Matches between one and unlimited times, as many times
         * as possible, giving back as needed (greedy)
         *
         * u modifier: unicode. Pattern strings are treated as UTF-16.
         * Also causes escape sequences to match unicode characters
         */
        $sHighlight = preg_replace('/[^\\w\\s\\&\\/\\"\\-\\+]+/u', '', rawurldecode($s));

        /*
         * Remove all characters not matching:
         *
         * \w match any word character in any script (equal to [\p{L}\p{N}_])
         * \s matches any kind of invisible character (equal to [\p{Z}\h\v])
         * \& matches the character & literally (case sensitive)
         * \/ matches the character / literally (case sensitive)
         * \" matches the character " literally (case sensitive)
         * \+ matches the character + literally (case sensitive)
         *
         * + Quantifier — Matches between one and unlimited times, as many times
         * as possible, giving back as needed (greedy)
         *
         * u modifier: unicode. Pattern strings are treated as UTF-16.
         * Also causes escape sequences to match unicode characters
         */
        $s = preg_replace('/[^\\w\\s\\&\\/\\"\\-\\+]+/u', '', rawurldecode($s));

        /*
         * Get quoted terms e.g. "Zubehör & Optionen"
         *
         * "([^"]+)"
         * " matches the character " literally (case sensitive)
         * 1st Capturing Group ([^"]+)
         * Match a single character not present in the list below [^"]+
         * " matches the character " literally (case sensitive)
         * " matches the character " literally (case sensitive)
         *
         * + Quantifier — Matches between one and unlimited times,
         * as many times as possible, giving back as needed (greedy)
         */
        preg_match_all('/"([^"]+)"/', $s, $aQuoted);
        if (!empty($aQuoted[0]) && !empty($aQuoted[1])) {
            foreach ($aQuoted[0] as $sValue) {
                $s = str_replace($sValue, '', $s);
            }
            $aSQuoted = $aQuoted[1];
        }

        $s = trim($s);
        if ($s != '') {
            /*
             * Remove leading + and - for each given search term
             */
            $aS = explode(' ', ltrim($s, "-+"));
            $aSHighlight = explode(' ', ltrim($sHighlight, "-+"));
        }

        $sWildcard = '';
        $sExact = '';
        $sWord = '';
        $sExactQuoted = '';
        if (!empty($aS)) {
            foreach ($aS as $k => $v) {
                $aS[$k] = rtrim($v, '+');
            }

            /*
             * +
             * A leading or trailing plus sign indicates that this word must be present
             * in each row that is returned. InnoDB only supports leading plus signs.
             *
             * *
             * The asterisk serves as the truncation (or wildcard) operator.
             * Unlike the other operators, it is appended to the word to be affected.
             * Words match if they begin with the word preceding the * operator.
             *
             * ()
             * Parentheses group words into subexpressions. Parenthesized groups can be nested.
             */
            $sWildcard = '(+' . implode('* +', $aS) . '*)';
            $sExact = '("' . implode(' ', $aS) . '")';
            $sWord = '(+' . implode(' +', $aS) . ')';
        }

        if (!empty($aSQuoted)) {
            foreach ($aSQuoted as $sQuoted) {
                $sQuoted = rtrim($sQuoted, '+');
                $sExactQuoted .= $sExactQuoted == '' ? '+("' . $sQuoted . '")' : ' +("' . $sQuoted . '")';
            }
        }

        $sTermsProcessed = $sWildcard . ' ' . $sExact . ' ' . $sWord . ' ' . $sExactQuoted;

        $sQuery =
            "SELECT
                *,
                MATCH (" . implode(",", $aFulltextColumns) . ") AGAINST (:s IN BOOLEAN MODE) AS score
            FROM
                " . $sIndex . "
            WHERE
                MATCH (" . implode(",", $aFulltextColumns) . ") AGAINST (:s IN BOOLEAN MODE)
            AND
                published = 1
            AND
                maintype IN (" . sprintf("'%s'", implode("','", array_unique($aMaintypes))) . ")
            AND
                type IN (" . sprintf("'%s'", implode("','", array_unique($aTypes))) . ")
            AND
                subtype IN (" . sprintf("'%s'", implode("','", $aSubtypes)) . ")
            ORDER BY
                score DESC
            ";

        if (trim($sTermsProcessed)) {

            $db = \Pimcore\Db::get();
            $aResults = $db->fetchAll(
                $sQuery,
                [ 's' => $sTermsProcessed]
            );

        }

        return [
            'aSHighlight' => $aSHighlight,
            'aResults' => $aResults,
        ];
    }

}
