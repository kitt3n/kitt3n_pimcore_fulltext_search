<?php

namespace KITT3N\Pimcore\FulltextSearchBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreFulltextSearchBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorefulltextsearch/js/pimcore/startup.js'
        ];
    }
}