<?php

namespace KITT3N\Pimcore\FulltextSearchBundle\Search;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class PreparedStatement
 *
 * @package KITT3N\Pimcore\FulltextSearchBundle\Search
 */
class PreparedStatement
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGeneratorInterface;

    /**
     * @var string
     */
    protected $searchTerm;

    /**
     * @var TypeDefinition[]
     */
    protected $typeDefinitions;

    /**
     * @var string[]
     */
    private $fulltextColumns = ['data', 'properties'];

    /**
     * @var string
     */
    private $indexTable = 'search_backend_data';


    /**
     * PreparedStatement constructor.
     *
     * @param string                 $searchTerm
     * @param UrlGeneratorInterface  $urlGeneratorInterface
     * @param array|TypeDefinition[] $typeDefinitions
     */
    public function __construct($searchTerm = '', UrlGeneratorInterface $urlGeneratorInterface, $typeDefinitions = [])
    {
        $this->searchTerm = $searchTerm;
        $this->urlGeneratorInterface = $urlGeneratorInterface;

        $this->typeDefinitions = $typeDefinitions;
        if (empty($typeDefinitions)) {
           $this->typeDefinitions[] = new TypeDefinition();
        }
    }

    public function getStatement()
    {
        $aConditions = [];

        if (empty($typeDefinitions)) {
            $this->typeDefinitions[] = new TypeDefinition();
        }

        foreach ($this->getTypeDefinitions() as $typeDefinition) {
            $aConditions[] =
                ' ( ' .
                'maintype = \'' . $typeDefinition->getMaintype() . '\'' .
                ' AND type = \'' . $typeDefinition->getType() . '\'' .
                ' AND subtype = \'' . $typeDefinition->getSubtype() . '\'' .
                ' ) ';
        }

        $sPreparedStatement =
            "SELECT
                *,
                MATCH (" . implode(",", $this->getFulltextColumns()) . ") AGAINST (:s IN BOOLEAN MODE) AS score
            FROM
                " . $this->getIndexTable() . "
            WHERE
                MATCH (" . implode(",", $this->getFulltextColumns()) . ") AGAINST (:s IN BOOLEAN MODE)
            AND
                published = 1
            AND
                (" . implode(" OR ", $aConditions) . ")
            ORDER BY
                score DESC
            ";

        return $sPreparedStatement;
    }

    public function getVariables()
    {
        if (empty($this->searchTerm)) {
            throw new \Exception('$searchTerm must not be empty');
        }

        $aS = [];
        $aSQuoted = [];

        /*
         * Remove all characters not matching:
         *
         * \w match any word character in any script (equal to [\p{L}\p{N}_])
         * \s matches any kind of invisible character (equal to [\p{Z}\h\v])
         * \& matches the character & literally (case sensitive)
         * \/ matches the character / literally (case sensitive)
         * \" matches the character " literally (case sensitive)
         * \+ matches the character + literally (case sensitive)
         *
         * + Quantifier — Matches between one and unlimited times, as many times
         * as possible, giving back as needed (greedy)
         *
         * u modifier: unicode. Pattern strings are treated as UTF-16.
         * Also causes escape sequences to match unicode characters
         */
        $s = preg_replace(
            '/[^\\w\\s\\&\\/\\"\\-\\+]+/u',
            '',
            rawurldecode(
                $this->searchTerm
            )
        );

        /*
         * Get quoted terms e.g. "Zubehör & Optionen"
         *
         * "([^"]+)"
         * " matches the character " literally (case sensitive)
         * 1st Capturing Group ([^"]+)
         * Match a single character not present in the list below [^"]+
         * " matches the character " literally (case sensitive)
         * " matches the character " literally (case sensitive)
         *
         * + Quantifier — Matches between one and unlimited times,
         * as many times as possible, giving back as needed (greedy)
         */
        preg_match_all('/"([^"]+)"/', $s, $aQuoted);
        if (!empty($aQuoted[0]) && !empty($aQuoted[1])) {
            foreach ($aQuoted[0] as $sValue) {
                $s = str_replace($sValue, '', $s);
            }
            $aSQuoted = $aQuoted[1];
        }

        $s = trim($s);
        if ($s != '') {
            /*
             * Remove leading + and - for each given search term
             */
            $aS = explode(' ', ltrim($s, "-+"));
        }

        $sWildcard = '';
        $sExact = '';
        $sWord = '';
        $sExactQuoted = '';
        if (!empty($aS)) {
            foreach ($aS as $k => $v) {
                $aS[$k] = rtrim($v, '+');
            }

            /*
             * +
             * A leading or trailing plus sign indicates that this word must be present
             * in each row that is returned. InnoDB only supports leading plus signs.
             *
             * *
             * The asterisk serves as the truncation (or wildcard) operator.
             * Unlike the other operators, it is appended to the word to be affected.
             * Words match if they begin with the word preceding the * operator.
             *
             * ()
             * Parentheses group words into subexpressions. Parenthesized groups can be nested.
             */
            $sWildcard = '(+' . implode('* +', $aS) . '*)';
            $sExact = '("' . implode(' ', $aS) . '")';
            $sWord = '(+' . implode(' +', $aS) . ')';
        }

        if (!empty($aSQuoted)) {
            foreach ($aSQuoted as $sQuoted) {
                $sQuoted = rtrim($sQuoted, '+');
                $sExactQuoted .= $sExactQuoted == '' ? '+("' . $sQuoted . '")' : ' +("' . $sQuoted . '")';
            }
        }

        $sTermsProcessed = $sWildcard . ' ' . $sExact . ' ' . $sWord . ' ' . $sExactQuoted;

        $aVariables = [
            's' => $sTermsProcessed
        ];

        return $aVariables;
    }

    public function execute()
    {
        $db = \Pimcore\Db::get();
        $results = $db->fetchAll(
            $this->getStatement(),
            $this->getVariables()
        );

        return $results;
    }

    public function executeAndProcess()
    {
        $db = \Pimcore\Db::get();
        $results = $db->fetchAll(
            $this->getStatement(),
            $this->getVariables()
        );

        if (empty($results)) {
            return $results;
        }

        foreach ($results as $key => $item) {
            $results[$key]['processed'] = [];
            foreach ($this->getTypeDefinitions() as $typeDefinition) {
                if ($typeDefinition->getMaintype() === $results[$key]['maintype'] and $typeDefinition->getType() === $results[$key]['type'] and $typeDefinition->getSubtype() === $results[$key]['subtype']) {

                    $fullyQualifiedClassName = $typeDefinition->getFullyQualifiedClassName();
                    if (!class_exists($fullyQualifiedClassName)) {
                        throw new \Exception('$fullyQualifiedClassName is not a valid class');
                    }
                    $results[$key]['processed']['object'] =
                        $fullyQualifiedClassName::getById(intval($results[$key]['id']));

                    $titleMethod = $typeDefinition->getTitleMethod();
                    if (!method_exists($results[$key]['processed']['object'], $titleMethod)) {
                        throw new \Exception('$titleMethod is not a valid method');
                    }
                    $results[$key]['processed']['title'] = $results[$key]['processed']['object']->$titleMethod();

                    $teaserMethod = $typeDefinition->getTeaserMethod();
                    if (!method_exists($results[$key]['processed']['object'], $teaserMethod)) {
                        throw new \Exception('$titleMethod is not a valid method');
                    }
                    $results[$key]['processed']['teaser'] = $results[$key]['processed']['object']->$teaserMethod();

                    $urlMethod = $typeDefinition->getUrlMethod();
                    $generateUrlRoute = $typeDefinition->getGenerateUrlRoute();
                    $generateUrlParameters = $typeDefinition->getGenerateUrlParameters();
                    switch (true) {
                        case is_null($urlMethod) and is_string($generateUrlRoute) and is_array($generateUrlParameters):

                            $urlParameters = [];
                            foreach ($generateUrlParameters as $urlParameterKey => $urlParameterValue) {

                                switch (true) {
                                    case is_string($urlParameterValue['value']) and is_null($urlParameterValue['method']):

                                        $urlParameters[$urlParameterKey] = $urlParameterValue['value'];

                                        break;
                                    case is_null($urlParameterValue['value']) and is_string($urlParameterValue['method'])
                                        and method_exists($results[$key]['processed']['object'],$urlParameterValue['method']):

                                        $methodName = $urlParameterValue['method'];
                                        $urlParameters[$urlParameterKey] =
                                            $results[$key]['processed']['object']->$methodName();

                                        break;
                                    default:
                                        throw new \Exception('$generateUrlParameters is not well configured');
                                }

                            }

                            $results[$key]['processed']['url'] =
                                $this->urlGeneratorInterface->generate(
                                    $generateUrlRoute,
                                    $urlParameters
                                );

                            break;
                        case is_string($urlMethod)
                            and method_exists($results[$key]['processed']['object'],$urlMethod)
                            and is_null($generateUrlRoute) and is_null($generateUrlParameters):

                            $methodName = $urlMethod;
                            $results[$key]['processed']['url'] = $results[$key]['processed']['object']->$methodName();

                            break;
                        default:
                            throw new \Exception('$urlMethod, $generateUrlRoute or $generateUrlParameters is not valid');
                    }

                    $results[$key]['processed']['subtype'] = $results[$key]['subtype'];

                }
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getSearchTerm(): string
    {
        return $this->searchTerm;
    }

    /**
     * @param string $searchTerm
     */
    public function setSearchTerm(string $searchTerm): void
    {
        $this->searchTerm = $searchTerm;
    }

    /**
     * @return TypeDefinition[]
     */
    public function getTypeDefinitions(): array
    {
        return $this->typeDefinitions;
    }

    /**
     * @param TypeDefinition[] $typeDefinitions
     */
    public function setTypeDefinitions(array $typeDefinitions): void
    {
        $this->typeDefinitions = $typeDefinitions;
    }

    /**
     * @param TypeDefinition[] $typeDefinitions
     */
    public function addTypeDefinitions(array $typeDefinitions): void
    {
        $this->typeDefinitions = array_merge($this->typeDefinitions, $typeDefinitions);
    }

    /**
     * @return string[]
     */
    public function getFulltextColumns(): array
    {
        return $this->fulltextColumns;
    }

    /**
     * @return string
     */
    public function getIndexTable(): string
    {
        return $this->indexTable;
    }
}
