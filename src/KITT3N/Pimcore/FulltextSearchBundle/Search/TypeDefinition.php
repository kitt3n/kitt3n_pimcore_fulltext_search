<?php

namespace KITT3N\Pimcore\FulltextSearchBundle\Search;

/**
 * Class TypeDefinition
 *
 * @package KITT3N\Pimcore\FulltextSearchBundle\Search
 */
class TypeDefinition
{
    /**
     * @var string
     */
    protected $maintype;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $subtype;

    /**
     * @var string
     */
    protected $fullyQualifiedClassName;

    /**
     * @var string
     */
    protected $titleMethod;

    /**
     * @var string
     */
    protected $teaserMethod;

    /**
     * @var string|null
     */
    protected $urlMethod;

    /**
     * @var string|null
     */
    protected $generateUrlRoute;

    /**
     * @var string[][]|null
     */
    protected $generateUrlParameters;


    /**
     * TypeDefinition constructor.
     *
     * @param string      $maintype
     * @param string      $type
     * @param string      $subtype
     * @param string      $fullyQualifiedClassName
     * @param string      $titleMethod
     * @param string      $teaserMethod
     * @param string|null $urlMethod
     * @param string|null $generateUrlRoute
     * @param string[][]|null  $generateUrlParameters
     */
    public function __construct(
        string $maintype = 'document',
        string $type = 'page',
        string $subtype = 'page',
        string $fullyQualifiedClassName = '\\Pimcore\\Model\\Document\\Page',
        string $titleMethod = 'getTitle',
        string $teaserMethod = 'getDescription',
        $urlMethod = 'getFullPath',
        $generateUrlRoute = null,
        $generateUrlParameters = null
    ) {
        $this->maintype = $maintype;
        $this->type = $type;
        $this->subtype = $subtype;
        $this->fullyQualifiedClassName = $fullyQualifiedClassName;
        $this->titleMethod = $titleMethod;
        $this->teaserMethod = $teaserMethod;
        $this->urlMethod = $urlMethod;
        $this->generateUrlRoute = $generateUrlRoute;
        $this->generateUrlParameters = $generateUrlParameters;
    }

    /**
     * @return string
     */
    public function getMaintype(): string
    {
        return $this->maintype;
    }

    /**
     * @param string $maintype
     */
    public function setMaintype(string $maintype): void
    {
        $this->maintype = $maintype;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSubtype(): string
    {
        return $this->subtype;
    }

    /**
     * @param string $subtype
     */
    public function setSubtype(string $subtype): void
    {
        $this->subtype = $subtype;
    }

    /**
     * @return string
     */
    public function getFullyQualifiedClassName(): string
    {
        return $this->fullyQualifiedClassName;
    }

    /**
     * @param string $fullyQualifiedClassName
     */
    public function setFullyQualifiedClassName(string $fullyQualifiedClassName): void
    {
        $this->fullyQualifiedClassName = $fullyQualifiedClassName;
    }

    /**
     * @return string
     */
    public function getTitleMethod(): string
    {
        return $this->titleMethod;
    }

    /**
     * @param string $titleMethod
     */
    public function setTitleMethod(string $titleMethod): void
    {
        $this->titleMethod = $titleMethod;
    }

    /**
     * @return string
     */
    public function getTeaserMethod(): string
    {
        return $this->teaserMethod;
    }

    /**
     * @param string $teaserMethod
     */
    public function setTeaserMethod(string $teaserMethod): void
    {
        $this->teaserMethod = $teaserMethod;
    }

    /**
     * @return string|null
     */
    public function getUrlMethod(): ?string
    {
        return $this->urlMethod;
    }

    /**
     * @param string|null $urlMethod
     */
    public function setUrlMethod(?string $urlMethod): void
    {
        $this->urlMethod = $urlMethod;
    }

    /**
     * @return string|null
     */
    public function getGenerateUrlRoute(): ?string
    {
        return $this->generateUrlRoute;
    }

    /**
     * @param string|null $generateUrlRoute
     */
    public function setGenerateUrlRoute(?string $generateUrlRoute): void
    {
        $this->generateUrlRoute = $generateUrlRoute;
    }

    /**
     * @return string[]|null
     */
    public function getGenerateUrlParameters(): ?array
    {
        return $this->generateUrlParameters;
    }

    /**
     * @param string[]|null $generateUrlParameters
     */
    public function setGenerateUrlParameters(?array $generateUrlParameters): void
    {
        $this->generateUrlParameters = $generateUrlParameters;
    }



}
