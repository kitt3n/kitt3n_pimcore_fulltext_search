pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreFulltextSearchBundle");

pimcore.plugin.Kitt3nPimcoreFulltextSearchBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreFulltextSearchBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreFulltextSearchBundle ready!");
    }
});

var Kitt3nPimcoreFulltextSearchBundlePlugin = new pimcore.plugin.Kitt3nPimcoreFulltextSearchBundle();
